.DEFAULT_GOAL := help
NO_COLOR=\033[0m
OK_COLOR=\033[32;01m
ERROR_COLOR=\033[31;01m
WARN_COLOR=\033[33;01m

build: ## Building Docker Image
	@echo "$(OK_COLOR)==> Building Docker Image$(NO_COLOR)"
	docker build -t flaviobrito/python_linter  .

push: ## Pushing the Image to Docker Hub
	@echo "$(WARN_COLOR)==> Building Docker Image$(NO_COLOR)"
	docker push flaviobrito/python_linter

.PHONY: help
help:
	@echo "$(OK_COLOR)==> List $(NO_COLOR)"
	@grep -E '^[a-zA-Z0-9_-]+:.*?## .*$$' $(MAKEFILE_LIST) \
	| sed -n 's/^\(.*\): \(.*\)##\(.*\)/\1\3/p' \
	| column -t  -s ' '
