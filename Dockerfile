FROM python:3.8-slim-buster
LABEL org.opencontainers.image.version="0.1" \
      maintainer="Flavio Brito" \
      org.opencontainers.image.authors="Flavio Brito" \
      release-date="2021-10-21" \
      org.opencontainers.image.description="This image is based on Debian slim-buster and contains official Python:3.8 and pylint, flake8 and sqlfluff" \
      org.opencontainers.image.licenses="Apache-2.0"

WORKDIR /data

RUN apt-get -y update && \
    apt-get -y install git && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /usr/share/man/?? /usr/share/man/??_*

COPY requirements.txt ./
RUN pip install --no-cache-dir --upgrade pip && \
    pip install --no-cache-dir -r requirements.txt

COPY pylintrc.cfg /etc/pylintrc.cfg