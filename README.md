# python-linter

# Overview
This image is based on Debian slim-buster and contains official Python: 3.8 and linters
# Description
Created to support Python developers to improve the Code Quality using CI/CD process on GitLab / GitHub. This image contains pylint, flake8 and sqlfluff that can be used on GitLab or GitHub CI/CD pipelines.

syntactical and stylistic problems in your Python source code,

# Usage
## GitLab CI/CD
Create a .gitlab-ci.yml file as:
```bash
image: flaviobrito/python_linter:latest

stages:
  - Code Analysis
  - SQL Analysis

pylint:
  stage: Code Analysis
  allow_failure: true
  script:
    - pylint --rcfile=/etc/pylintrc.cfg ./*.py

flake8:
  stage: Code Analysis
  allow_failure: true
  script:
    - flake8  --count --select=E9,F63,F7,F82 --show-source --statistics ./
    - flake8  --count --max-complexity=10 --max-line-length=120 --statistics ./

sqllint:
  stage: SQL Analysis
  before_script:
    - pip3 install -q -U sqlfluff
  allow_failure: true
  script:
    - sqlfluff lint --dialect postgres
```